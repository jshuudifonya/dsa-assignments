import ballerina/grpc;
import ballerina/log;


@grpc:ServiceDescriptor {
    descriptor: "ROOT_DESCRIPTOR",
    descMap: addFun()
}
service "HelloWorld" on new grpc:Listener(9090) {
    remote function addFun(addFunRequest) returns addFunResponse|error{

    }