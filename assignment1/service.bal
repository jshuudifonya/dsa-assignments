import ballerina/http;
import ballerina/io;

type infor record{
    string name;
    string chapters;
    byte recordings;
};

public function main() returns error? {

http:Client httpClient=check new("https://varsity-learning.portal.com");

    infor payload= check httpClient->get("learning-materials");
    io:println(payload);
}
service / on new http:Listener(9090) {
    resource function get getResults(string? symbol) returns json{
        io:println("Course symbol");
    }

}