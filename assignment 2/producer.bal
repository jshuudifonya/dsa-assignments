import ballerinax/kafka;

kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL);
public function main() returns error? {
    string message = "Welcome to kafka";
    check kafkaProducer->send({
                                topic: "dsa-assignment",
                                value: message.toBytes() });

    check kafkaProducer->'flush();
}