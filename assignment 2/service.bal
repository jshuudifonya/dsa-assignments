import ballerinax/kafka;
import ballerina/log;

kafka:ConsumerConfiguration consumerConfigs = {
   groupId: "brokerId",
    topics: ["dsa-assignment"],

    pollingInterval: 1,
    autoCommit: false
};


listener kafka:Listener kafkaListener =
        new (kafka:DEFAULT_URL, consumerConfigs);
        


service kafka:Service on kafkaListener {
    remote function onConsumerRecord(kafka:Caller caller,
                                kafka:ConsumerRecord[] records) returns error? {
        foreach var kafkaRecord in records {
            check processKafkaRecord(kafkaRecord);
        }

        kafka:Error? serviceResult = caller->commit();

        if serviceResult is error {
            log:printError("Error occurred while committing the " +
                "offsets for the consumer ", 'error = serviceResult);
        }
    }
}
function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) returns error? {
    byte[] value = kafkaRecord.value;

    string messageContent = check string:fromBytes(value);
    log:printInfo("Received Message: " + messageContent);
}